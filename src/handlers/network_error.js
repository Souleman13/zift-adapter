export const netErr = (r, res) => {
    res.send({
        'message': 'Processing Network Error',
        'reason': providerResponseMessage,
        'code': responseCode, //could be any of the above case codes
        'responseBody': r //body of zift api response in json
    })
}