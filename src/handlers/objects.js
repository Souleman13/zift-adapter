//required incoming json object from requester to zift adapter api
const request_transaction = {
    //merchant account?
    //transaction type-  cc/debit/ach/etc
    'amount': '',
    'holderName': '', //name on card or account
    'holderBirthdate': '', //birthdate of card or account holder
    'accountType': '', //ach only
    'accountNumber': '', //card or acc #
    'accountAccessory': '', //expiration date or routing number
    'csc': '', //card only
    'street': '', //basic address info...
    'city': '',
    'state': '',
    'zip': ''
}

//response object from zift adapter api to requester
const response = {
    'message': '',
    'reason': '',
    'id': '',
    'code': '',
    'responseBody': {} 
}