import {response} from './objects'

export const Approved = async (r,res) => {
    await res.send({
        'message':'Transaction Approved',
        'amount': r.amount,
        'reason': r.responseMessage, //Zift response message
        'id': r.transactionId, //zift transactions reference number
        'code': r.responseCode, //zift response code
        'responseBody': r //full body of zift api response in json
    })
}

export const PartiallyApproved = async (r, res) => {
    await res.send({
        'message':'Partial Payment Transaction Approved',
        'amount': r.amount,
        'reason': r.responseMessage,
        'id': r.transactionId,
        'code': r.responseCode,
        'responseBody': r
    })
}

export const CreditPosted = async (r, res) => {
    return await res.send({
        'message':'Credit Posted',
        'amount': r.amount,
        'reason': r.responseMessage,
        'id': r.transactionId,
        'code': r.responseCode, 
        'responseBody': r
    })
}

export const Void = async (r, res) => {
    return await res.send({
        'message':'Void',
        'amount': r.voidAmount,
        'reason': r.responseMessage,
        'id': r.transactionId,
        'code': r.responseCode, 
        'responseBody': r
    })
}

export const Refund = async (r, res) => {
    return await res.send({
        'message':'Refund',
        'amount': r.voidAmount,
        'reason': r.responseMessage,
        'id': r.transactionId,
        'code': r.responseCode, 
        'responseBody': r
    })
}