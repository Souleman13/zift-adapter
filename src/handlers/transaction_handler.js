import qs from 'qs'
import * as approved from './approved'
import * as badCC from './bad_cc'
import * as badACH from './bad_ach'
import * as networkError from './network_error'

export const TrannyHandler = async (body, res) => {
    const r = await qs.parse(body)

    const { responseCode, providerResponseMessage } = r

    switch (responseCode) {
        //approved transaction cases
        case 'A01': //full approval
            return await approved.Approved(r, res)
            break
        //credit posted
        case 'A02':
            console.log('switch')
            return await approved.CreditPosted(r, res)
            break
        case 'A05': //partial approval
            return await approved.PartiallyApproved(r, res)
            break
        case 'A03': //void or refund posted, auth reversed
        case 'A04': //no update
        case 'A06': //void or refund posted, auth no reversed
        case 'A07': //partial void posted
        case 'A08': //partial refund posted
            if(r.responseType === "refund") return await approved.Refund(r, res)
            if(r.responseType === "void") return await approved.Void(r, res)
            break
        case 'A09': //incremental auth posted
        case 'A10': //request accepted
        case 'A11': //approval (reversal failed)
            return await res.send('unhandled approved case')
            break

        //bad card transaction cases
        case 'D05': //invalid card
        case 'D10': //card reported lost
        case 'D30': //call for auth
        case 'D04': //hold - pick up card
        case 'D08': //csc invalid
        case 'D03': //insuffecient funds
        case 'D08': //chargeback recieved
            return await res.send({
                'message': 'Credit Card Transaction Denied',
                'reason': providerResponseMessage,
                'code': responseCode, //could be any of the above case codes
                'responseBody': r //body of zift api response in json
            })
            break

        //bad ACH transaction cases
        case 'R05': //Unauthorized Debit to Consumer Account Using Corporate SEC Code
        case 'R02': //Account Closed
        case 'R08': //Stop Payment or Stop on Source Document
        case 'R16': //Account Frozen
        case 'R03': //No Account
        case 'R01': //Insufficient Funds
            return await res.send({
                'message': 'ACH Transaction Denied',
                'reason': providerResponseMessage,
                'code': responseCode, //could be any of the above case codes
                'responseBody': r //body of zift api response in json
            })
            break

        //processing network errors
        case 'E02': //network unavailable
        case 'E09': //network error
            return await networkError.netErr(r, res)
            break
        
        //default for no matching responseCode
        default: return await res.send('unhandled transaction response code')
    }
}