//zift docs => http://api.ziftpay.com/index.php

//zift transaction endpoints
export const sandboxTransaction = 'https://sandbox-secure.ziftpay.com/gates/xurl?'
export const productionTransaction = 'https://secure.ziftpay.com/gates/xurl?'
//zift onboarding
export const sandboxOnboarding = 'https://manage.ziftpay.com/gates/onboarding?'
export const productionOnboarding = 'https://sandbox-manage.ziftpay.com/gates/onboarding?'
//ZIFT CREDENTIALS
//transactions
export const usernameTransaction = 'api_blytz_rs83_trans'
export const passwordTransaction = 'e8OK44OGbFPnmAdk4QXGOB2S2t9RbCs3'
//onboarding
export const usernameOnboarding = 'api_blytz_rs83_onb'
export const passwordOnboarding = 'Vl5daki2sgApPuOQg4eU9Q2Ehv4DlcQC'
//merchant account #s
export const merchantAcc1 = '1214001'
export const merchantAcc2 = '1215001'

//onboarding information
export const resellerId = '83'
export const portfolioId = '100'
export const profileId = '1943779842'
export const feeTemplateId = '100026'
export const processingConfigurationScript = 'zift11'
export const merchantProfile = 'SSSSM'