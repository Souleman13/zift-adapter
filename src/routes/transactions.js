import { Router } from 'express'
import request from 'request'
import { sandboxTransaction, usernameTransaction, passwordTransaction, merchantAcc1 } from '../zift_info'
import { TrannyHandler } from '../handlers/transaction_handler' //transaction handler function

const url = sandboxTransaction
const routes = Router()

//routes for payments transactions

//credit card
routes.post('/cc', async (req, res) => {

    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'sale',
            'userName': usernameTransaction,
            'password': passwordTransaction,
            'accountId': merchantAcc1,
            'amount': req.body.amount, //in cents ex. $5.00 = 500 cents
            'accountType': 'R', //ENUM value for credit card
            'transactionIndustryType': 'RE', //ENUM value for retail
            'holderType': 'P', //P for personal account
            'holderName': req.body.holderName, //name on card
            'holderBirthdate': req.body.holderBirthdate, //birthdate of card holder
            'accountNumber': req.body.accountNumber, //card number
            'accountAccessory': req.body.accountAccessory, //expiration date
            'csc': req.body.csc, //3 digit code on back
            //address info
            'street': req.body.street,
            'city': req.body.city,
            'state': req.body.state,
            'zipCode': req.body.zipCode,
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await TrannyHandler(body, res)
        }
    })
})

//ach
routes.post('/ach', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'sale',
            'userName': usernameTransaction,
            'password': passwordTransaction,
            'accountId': merchantAcc1,
            //transactionfields
            'amount': req.body.amount, //in cents ex. $5.00 = 500 cents
            'transactionIndustryType': 'RE', //ENUM for retail
            'accountType': req.body.accountType, //C for checking, S for savings
            'accountNumber': req.body.accountNumber,
            'accountAccessory': req.body.accountAccessory, //routing #
            'holderName': req.body.holderName, //name on account
            'holderBirthdate': req.body.holderBirthdate, //birthdate of account holder            
            //address info
            'street': req.body.street,
            'city': req.body.city,
            'state': req.body.state,
            'zipCode': req.body.zipCode,
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await TrannyHandler(body, res)
        }
    })
})

//credit bank account
routes.post('/credit', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'credit',
            'userName': usernameTransaction,
            'password': passwordTransaction,
            'accountId': merchantAcc1,
            //transactionfields
            'amount': req.body.amount, //in cents ex. $5.00 = 500 cents
            'transactionIndustryType': 'RE', //ENUM for retail
            'accountType': req.body.accountType, //C for checking, S for savings
            'accountNumber': req.body.accountNumber,
            'accountAccessory': req.body.accountAccessory, //routing #
            'holderName': req.body.holderName, //name on account
            'holderBirthdate': req.body.holderBirthdate, //birthdate of account holder            
            //address info
            'street': req.body.street,
            'city': req.body.city,
            'state': req.body.state,
            'zipCode': req.body.zipCode,
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await TrannyHandler(body, res)
        }
    })
})

//void trans
routes.post('/void', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'void',
            'userName': usernameTransaction,
            'password': passwordTransaction,
            'accountId': merchantAcc1,
            'amount': req.body.amount,
            'transactionId': req.body.transactionId
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await TrannyHandler(body, res)
            // await TrannyHandler(body, res)
        }
    })
})

//refund trans
routes.post('/refund', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'refund',
            'userName': usernameTransaction,
            'password': passwordTransaction,
            'accountId': merchantAcc1,
            'amount': req.body.amount,
            'transactionId': req.body.transactionId
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await TrannyHandler(body, res)
        }
    })
})

export default routes