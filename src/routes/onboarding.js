import { Router } from 'express'
import request from 'request'
import {
    sandboxOnboarding,
    usernameOnboarding,
    passwordOnboarding,
    resellerId,
    portfolioId,
    profileId,
    feeTemplateId,
    processingConfigurationScript,
    merchantProfile
} from '../zift_info'

const url = sandboxOnboarding
const routes = Router()

routes.post('/create', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'create',
            'userName': usernameOnboarding,
            'password': passwordOnboarding,
            'merchantProfile': merchantProfile,
            'profileId': profileId,
            'resellerId': resellerId,
            'portfolioId': portfolioId,
            'feeTemplateId': feeTemplateId,
            'processingConfigurationScript': processingConfigurationScript,
            'isEmbedded': '1',
            'pageFormat': 'OBDA',
            'owner.firstName': 'Tony',
            'owner.lastName': 'Stark',
            'owner.email': 'tony@ziftpay.com',
            'owner.street1': '10880 Malibu Point',
            'owner.street2': '',
            'owner.city': 'Los Angeles',
            'owner.state': 'CA',
            'owner.zipCode': '95660',
            'owner.countryCode': 'US',
            'owner.birthDate': '19670210',
            'owner.socialSecurity': '369258741',
            'owner.phone': '5551234567',
            'business.businessName': 'Stark Labs',
            'business.legalName': 'Stark Industries',
            'business.ownershipStructureType': 'C',
            'business.street1': '34th Street Stark Tower',
            'business.street2': '',
            'business.city': 'New York',
            'business.state': 'NY',
            'business.zipCode': '10025',
            'business.countryCode': 'US',
            'business.descriptorPhone': '5551234567',
            'business.paymentCardDescriptor': 'starkind-1234567890',
            'business.directDebitDescriptor': 'starkind:1234567890',
            'business.taxId': '741852369',
            'business.webSite': 'http://stark.ziftpay.com',
            'business.email': 'tony@ziftpay.com',
            'business.description': 'Aerospace things and really cool stuff',
            'business.merchantCategoryCode': '7399',
            'business.currencyCode': 'USD',
            'business.contactPhone': '1234567890',
            'business.timeZoneCode': '',
            'business.relationshipBeginDate': '',
            'estimates.annualDirectDebitVolume': '1000000',
            'estimates.annualCardsVolume': '1000000',
            'estimates.avgDirectDebitTransactionAmount': '400000',
            'estimates.avgCardsTransactionAmount': '600000',
            'estimates.maxTransactionAmount': '500000',
            'deposit.bankName': 'Stark Bank',
            'deposit.holderName': 'Tony Stark',
            'deposit.routingNumber': '324377516',
            'deposit.accountNumber': '1234567980',
            'deposit.accountType': 'C'
        }
    }

    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            await res.send(body)
        }
    })
})

export default routes