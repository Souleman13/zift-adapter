import { Router } from 'express'
import request from 'request'
import { sandboxTransaction } from '../zift_info'
import { TrannyHandler } from '../handlers/transaction_handler'

const url = sandboxTransaction

const routes = Router()

//the next set of tests will send the following
//hardcode cc request to zift
//the different amounts in each test will force
//specific error responses from the server
//ex. invalid card or card reported stolen
//then test to error handler for that response
//each test returns body of the zift api response
const o = {
    url: sandboxTransaction,
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    form: {
        'requestType': 'sale',
        'userName': 'api_blytz_rs83_trans',
        'password': 'e8OK44OGbFPnmAdk4QXGOB2S2t9RbCs3',
        'accountId': '1214001',
        'amount': '150000', //changes every test
        'accountType': 'R', //ENUM value for credit card
        'transactionIndustryType': 'RE', //ENUM value for retail
        'holderType': 'P', //P for personal account
        'holderName': 'John Smith', //name on card
        'accountNumber': '4111111111111111', //card number
        'accountAccessory': '0422', //expiration date
        //address info
        'street': '12 Main St',
        'city': 'Denver',
        'state': 'CO',
        'zipCode': '30301',
        //external identifiers
        'customerAccountCode': '0000000001',
        'transactionCode': '0000000001'
    }
}

//returns 'Approved' transaction response body
routes.post('/cc/1', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'sale',
            'userName': 'api_blytz_rs83_trans',
            'password': 'e8OK44OGbFPnmAdk4QXGOB2S2t9RbCs3',
            'accountId': '1214001',
            'amount': '150000',
            'accountType': 'R', //ENUM value for credit card
            'transactionIndustryType': 'RE', //ENUM value for retail
            'holderType': 'P', //P for personal account
            'holderName': 'John Smith', //name on card
            'accountNumber': '4111111111111111', //card number
            'accountAccessory': '0422', //expiration date
            //address info
            'street': '12 Main St',
            'city': 'Denver',
            'state': 'CO',
            'zipCode': '30301',
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }
    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            // await console.log(body)
            await res.send(body)
        } if (error) throw error
    })
})

//test of generic error handler, looks at response code for A01, the approved transaction
//should respond with string 'transaction failed'
routes.post('/cc/2', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'sale',
            'userName': 'api_blytz_rs83_trans',
            'password': 'e8OK44OGbFPnmAdk4QXGOB2S2t9RbCs3',
            'accountId': '1214001',
            'amount': req.body.amount,
            'accountType': 'R', //ENUM value for credit card
            'transactionIndustryType': 'RE', //ENUM value for retail
            'holderType': 'P', //P for personal account
            'holderName': 'John Smith', //name on card
            'accountNumber': '4111111111111111', //card number
            'accountAccessory': '0422', //expiration date
            //address info
            'street': '12 Main St',
            'city': 'Denver',
            'state': 'CO',
            'zipCode': '30301',
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }
    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            //console.log(body)
            const r = await qs.parse(body)
            if (r.responseCode !== 'A01' || 'A05') { //A01 is response code for all approved transactions, A05 is partial approval
                //this is where transaction error handlers go
                return res.send('transaction failed')
            }
            await res.send(r.providerResponseMessage)
        } if (error) throw error
    })
})

//final test end point, use different amounts to trigger different handlers.
//For more on what amounts trigger which codes: http://docs.ziftpay.com/processing-api/test_ranges
//testing transaction handler function for credit card responses
routes.post('/cc/test', async (req, res) => {
    const options = {
        url: url,
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        form: {
            'requestType': 'sale',
            'userName': 'api_blytz_rs83_trans',
            'password': 'e8OK44OGbFPnmAdk4QXGOB2S2t9RbCs3',
            'accountId': '1214001',
            'amount': req.body.amount,
            'accountType': 'R', //ENUM value for credit card
            'transactionIndustryType': 'RE', //ENUM value for retail
            'holderType': 'P', //P for personal account
            'holderName': 'John Smith', //name on card
            'accountNumber': '4111111111111111', //card number
            'accountAccessory': '0422', //expiration date
            //address info
            'street': '12 Main St',
            'city': 'Denver',
            'state': 'CO',
            'zipCode': '30301',
            //external identifiers
            'customerAccountCode': '0000000001',
            'transactionCode': '0000000001'
        }
    }
    await request(options, async (error, response, body) => {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            //console.log(body)
            await TrannyHandler(body, res)
        } if (error) throw error
    })
})

export default routes